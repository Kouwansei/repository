﻿using System;

namespace LabFeatureBranch
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            int Num;
            int Sum = 0;
            EnterNum:

            Console.Write("Please enter a number: ");
            if (!Int32.TryParse(Console.ReadLine(), out Num))
            {
                Console.WriteLine("Not a number!");
                goto EnterNum;
            }
            Console.Write("\n\n");
            Console.Write("Natural number of: "+Num+"\n");
            Console.Write("---------------------------------------");
            Console.Write("\n\n");
            Console.WriteLine("The natural number are:");
            for (int i = 1; i <= Num;i++) 
            {
                Console.Write("{0} ", i);
            }
            Console.Write("\n\n");

            for (int i = 1; i <= Num; i++)
            {
                Sum += i;
            }
            Console.Write("The sum is: " + Sum + "\n\n");
        

            GetSqNum(Num);
        }

        private static void GetSqNum(int Num)
        {
            for (int i = 1; i <= Num; i++)
            {
                Console.Write(string.Format("{0} ", i * i));
            }
        }
    }
}
